PVector target;
PVector[] obstacle;
ArrayList<Missile> gen;
int popSize = 2000;
int generation = 1;
boolean completed = false;

void setup() {

  size(600, 600);
  target = new PVector(width / 2, 100);
  obstacle = new PVector[2];
  obstacle[0] = new PVector(width / 2 - 200, height / 2 + 13);
  obstacle[1] = new PVector(width / 2 + 200, height / 2 - 13);
  gen = new ArrayList<Missile>();
  for (int i = 0; i < popSize; i++) {
    gen.add(new Missile(target, obstacle));
  }
}

void draw() {
  if (frameCount % 200 == 0) {
    repopulate();
  }
  update();
}

void update() {
  background(21);
  noStroke();
  fill(255);
  textSize(20);
  textAlign(LEFT, CENTER);
  text("generation - " + generation, 15, 25);
  text("population - " + popSize, 15, 50);
  if (!completed)
    fill(255, 0, 0);
  else
    fill(0, 255, 0);
  text("completed - " + completed, 15, 75);
  fill(51);
  rectMode(CENTER);
  rect(width / 2, height / 2, 400, 26);
  fill(255, 0, 0);
  ellipse(target.x, target.y, 50, 50);
  fill(255);
  ellipse(target.x, target.y, 35, 35);
  fill(255, 0, 0);
  ellipse(target.x, target.y, 15, 15);
  for (Missile m : gen) {
    m.show();
  }
}

void repopulate() {
  float sum = 0;
  for (Missile m : gen) {
    m.calcFitness();
    sum += m.fitness;
  }

  //it becomes a percentage of the total fitness of the generation
  for (Missile m : gen) {
    m.fitness /= sum;
  }

  //repopulating
  ArrayList<Missile> temp = new ArrayList<Missile>();
  Collections.sort(gen, new SortByFitness());
  for (int i = 0; i < popSize; i++) {
    temp.add(new Missile(gen, target, obstacle));
  }
  gen = temp;
  generation++;
  completed = false;
}
