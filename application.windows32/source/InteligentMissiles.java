import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class InteligentMissiles extends PApplet {

PVector target;
PVector[] obstacle;
ArrayList<Missile> gen;
int popSize = 2000;
int generation = 1;
boolean completed = false;

public void setup() {

  
  target = new PVector(width / 2, 100);
  obstacle = new PVector[2];
  obstacle[0] = new PVector(width / 2 - 200, height / 2 + 13);
  obstacle[1] = new PVector(width / 2 + 200, height / 2 - 13);
  gen = new ArrayList<Missile>();
  for (int i = 0; i < popSize; i++) {
    gen.add(new Missile(target, obstacle));
  }
}

public void draw() {
  if (frameCount % 200 == 0) {
    repopulate();
  }
  update();
}

public void update() {
  background(21);
  noStroke();
  fill(255);
  textSize(20);
  textAlign(LEFT, CENTER);
  text("generation - " + generation, 15, 25);
  text("population - " + popSize, 15, 50);
  if (!completed)
    fill(255, 0, 0);
  else
    fill(0, 255, 0);
  text("completed - " + completed, 15, 75);
  fill(51);
  rectMode(CENTER);
  rect(width / 2, height / 2, 400, 26);
  fill(255, 0, 0);
  ellipse(target.x, target.y, 50, 50);
  fill(255);
  ellipse(target.x, target.y, 35, 35);
  fill(255, 0, 0);
  ellipse(target.x, target.y, 15, 15);
  for (Missile m : gen) {
    m.show();
  }
}

public void repopulate() {
  float sum = 0;
  for (Missile m : gen) {
    m.calcFitness();
    sum += m.fitness;
  }

  //it becomes a percentage of the total fitness of the generation
  for (Missile m : gen) {
    m.fitness /= sum;
  }

  //repopulating
  ArrayList<Missile> temp = new ArrayList<Missile>();
  Collections.sort(gen, new SortByFitness());
  for (int i = 0; i < popSize; i++) {
    temp.add(new Missile(gen, target, obstacle));
  }
  gen = temp;
  generation++;
  completed = false;
}

class Missile implements Comparable<Missile> {
  //Own variables
  ArrayList<PVector> thrusts;
  PVector vel, pos;
  int movQ = 200, speed = 5;
  float fitness;
  float angle;
  boolean dead = false, winner = false;
  int col;
  float multiplier;
  boolean changedCol = false;

  //map variables
  PVector target;
  PVector[] obstacle;
  Missile(PVector target, PVector[] obstacle) {
    this.target = target;
    this.obstacle = obstacle;
    //negative y goes upwards
    vel = new PVector(0, -1);
    pos = new PVector(width / 2, height - 50);
    col = randomCol();
    thrusts = new ArrayList<PVector>();
    for (int i = 0; i < movQ; i++) {
      thrusts.add(PVector.random2D());
    }
  }

  Missile(ArrayList<Missile> prevGen, PVector target, PVector[] obstacle) {
    this.target = target;
    this.obstacle = obstacle;
    //negative y goes upwards
    vel = new PVector(0, -1);
    pos = new PVector(width / 2, height - 50);
    thrusts = new ArrayList<PVector>();
    //genetic algorithm
    float rnd = random(1);
    Missile parent1 = prevGen.get(pickMissile(rnd, prevGen));
    Missile parent2 = prevGen.get(pickMissile(rnd, prevGen));
    col = color((red(parent1.col) + red(parent2.col)) / 2, (green(parent1.col) + green(parent2.col)) / 2, (blue(parent1.col) + blue(parent2.col)) / 2);
    for (int i = 0; i < movQ; i++) {
      if (random(1) >= 0.03f) {
        if (random(1) >= 0.5f)
          this.thrusts.add(parent1.thrusts.get(i));
        else
          this.thrusts.add(parent2.thrusts.get(i));
      } else {
        this.thrusts.add(PVector.random2D());
        if (!changedCol)
          col = color((4 * red(col) + random(255)) / 5, (4 * green(col) + random(255)) / 5, (4 * blue(col) + random(255)) / 5);
        changedCol = true;
      }
    }
  }

  public void calcFitness() {
    fitness = 1 / (sq(dist(pos.x, pos.y, target.x, target.y)));
    if (dead)
      fitness /= 100; 
    if (winner)
      fitness *= multiplier;
  }

  public void show() {
    fill(255, 100);
    //rotates the Missile to its velocity angle
    pushMatrix();
    move();
    translate(pos.x, pos.y);
    rotate(-(HALF_PI - vel.heading()));
    rect(0, -16, 8, 40);
    //draws a "head"
    fill(col);
    rect(0, 0, 8, 8);
    popMatrix();
  }

  public void move() {

    checkState();
    if (!winner && !dead) {
      vel.add(thrusts.get(frameCount % 200)).setMag(speed);
      pos.add(vel);
      angle = vel.heading();
    }
  }

  public void checkState() {
    if ((pos.x >= obstacle[0].x && pos.y <= obstacle[0].y)) {
      if ((pos.x <= obstacle[1].x && pos.y >= obstacle[1].y)) {
        dead = true;
      }
    } 
    if (dist(pos.x, pos.y, target.x, target.y) <= 28) {
      winner = true;
      multiplier = 400 / (frameCount % 200);
      completed = true;
    }
  }

  public int randomCol() {
    int c = color(random(255), random(255), random(255));
    return c;
  }

  public int compareTo(Missile m2) {
    return floor(abs(fitness - m2.fitness) / (fitness - m2.fitness));
  }

  public int pickMissile(float rnd, ArrayList<Missile> list) {
    int index = 0;
    float comp = 1;
    for (int i = 0; i < list.size(); i++) {
      comp -= list.get(i).fitness;
      if (comp <= rnd) {
        index = i;
        break;
      }
    }
    return index;
  }
}
class SortByFitness implements Comparator<Missile>{
  public int compare(Missile m1, Missile m2){
    return floor(abs(m2.fitness - m1.fitness) / (m2.fitness - m1.fitness));
  }
}
  public void settings() {  size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "InteligentMissiles" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
