class SortByFitness implements Comparator<Missile>{
  public int compare(Missile m1, Missile m2){
    return floor(abs(m2.fitness - m1.fitness) / (m2.fitness - m1.fitness));
  }
}
