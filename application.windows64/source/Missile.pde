import java.util.*;
class Missile implements Comparable<Missile> {
  //Own variables
  ArrayList<PVector> thrusts;
  PVector vel, pos;
  int movQ = 200, speed = 5;
  float fitness;
  float angle;
  boolean dead = false, winner = false;
  color col;
  float multiplier;
  boolean changedCol = false;

  //map variables
  PVector target;
  PVector[] obstacle;
  Missile(PVector target, PVector[] obstacle) {
    this.target = target;
    this.obstacle = obstacle;
    //negative y goes upwards
    vel = new PVector(0, -1);
    pos = new PVector(width / 2, height - 50);
    col = randomCol();
    thrusts = new ArrayList<PVector>();
    for (int i = 0; i < movQ; i++) {
      thrusts.add(PVector.random2D());
    }
  }

  Missile(ArrayList<Missile> prevGen, PVector target, PVector[] obstacle) {
    this.target = target;
    this.obstacle = obstacle;
    //negative y goes upwards
    vel = new PVector(0, -1);
    pos = new PVector(width / 2, height - 50);
    thrusts = new ArrayList<PVector>();
    //genetic algorithm
    float rnd = random(1);
    Missile parent1 = prevGen.get(pickMissile(rnd, prevGen));
    Missile parent2 = prevGen.get(pickMissile(rnd, prevGen));
    col = color((red(parent1.col) + red(parent2.col)) / 2, (green(parent1.col) + green(parent2.col)) / 2, (blue(parent1.col) + blue(parent2.col)) / 2);
    for (int i = 0; i < movQ; i++) {
      if (random(1) >= 0.03) {
        if (random(1) >= 0.5)
          this.thrusts.add(parent1.thrusts.get(i));
        else
          this.thrusts.add(parent2.thrusts.get(i));
      } else {
        this.thrusts.add(PVector.random2D());
        if (!changedCol)
          col = color((4 * red(col) + random(255)) / 5, (4 * green(col) + random(255)) / 5, (4 * blue(col) + random(255)) / 5);
        changedCol = true;
      }
    }
  }

  void calcFitness() {
    fitness = 1 / (sq(dist(pos.x, pos.y, target.x, target.y)));
    if (dead)
      fitness /= 100; 
    if (winner)
      fitness *= multiplier;
  }

  void show() {
    fill(255, 100);
    //rotates the Missile to its velocity angle
    pushMatrix();
    move();
    translate(pos.x, pos.y);
    rotate(-(HALF_PI - vel.heading()));
    rect(0, -16, 8, 40);
    //draws a "head"
    fill(col);
    rect(0, 0, 8, 8);
    popMatrix();
  }

  void move() {

    checkState();
    if (!winner && !dead) {
      vel.add(thrusts.get(frameCount % 200)).setMag(speed);
      pos.add(vel);
      angle = vel.heading();
    }
  }

  void checkState() {
    if ((pos.x >= obstacle[0].x && pos.y <= obstacle[0].y)) {
      if ((pos.x <= obstacle[1].x && pos.y >= obstacle[1].y)) {
        dead = true;
      }
    } 
    if (dist(pos.x, pos.y, target.x, target.y) <= 28) {
      winner = true;
      multiplier = 400 / (frameCount % 200);
      completed = true;
    }
  }

  color randomCol() {
    color c = color(random(255), random(255), random(255));
    return c;
  }

  public int compareTo(Missile m2) {
    return floor(abs(fitness - m2.fitness) / (fitness - m2.fitness));
  }

  int pickMissile(float rnd, ArrayList<Missile> list) {
    int index = 0;
    float comp = 1;
    for (int i = 0; i < list.size(); i++) {
      comp -= list.get(i).fitness;
      if (comp <= rnd) {
        index = i;
        break;
      }
    }
    return index;
  }
}
